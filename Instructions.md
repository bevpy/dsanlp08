# Code Repeatability Instructions #

###Key notebooks for DSA Capstone Project###

NOTE: This is just showing the sequential steps for running code. For our data story/insights, check out our data story presentation.

Step 1: Scrape the data
	You can find the notebook here: TEST/final_scraping

Step 2: Upload the data to the database, perform sentiment analysis, and extract articles containing �opioid�
	You can find the notebook here: TEST/CSV Processing with Sentiment - Milestone 8

Step 3: Extract articles containing �opioid�. Manually label the opioid articles according to topic class. 
If classes unbalanced, find other articles to add to help balance out. 
You can find the notebook here: TEST/db_query_opioidFinal.ipynb

Step 4: Upload location data to the database 
	You can find the notebook here: TEST/Location Upload - Milestone 8 

Step 5: General Exploratory Analysis
	You can find the notebook here: TEST/Opioid Analysis - Milestone 8 

Step 6: Location Analysis 
	You can find the notebook here: TEST/Opioid Analysis - Milestone 8 - R Visualizations - Long

Step 7: Topic Modeling
	You can find the notebook here: TEST/Topic Modeling - Gensim - Opioid Articles


Step 8: Supervised Learning on Opioid Articles
	You can find the notebook here: TEST/supervised_learning_modelsFinal.ipynb


## Libraries needed:
Our IT managers downloaded Anaconda and the base libraries for R and Python, but you will also need these additional libraries:

### Python: 
lda
Gensim
Spacy
TextBlob
Nltk
Beautifulsoup
Sqlite3 
seaborn

### R: 
ggmap 
Usmap
DBI
dplyr
Rgooglemap
rsconnect
Shiny
